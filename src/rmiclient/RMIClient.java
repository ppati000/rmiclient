/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rmiclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import rmiserver.Server;

/**
 * Der Client für den RMI Server.
 * @author Patrick P.
 * @version 0.1
 */
public class RMIClient {

    /**
     * @param args none
     */
    public static void main(String[] args) {
        try {
            
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Bitte Host eingeben: ");
            String host = br.readLine();
            System.out.println("Bitte Port eingeben: ");
            String port = br.readLine();
            
            System.out.println("Verbinde mit Registry von " + host + "über Port " + port);
            
            //Mit der Registry des Servers verbinden und "server" anfordern
            Registry hostRegistry = LocateRegistry.getRegistry(host, Integer.valueOf(port));
            Server serverStub = (Server) hostRegistry.lookup("server");
            
            System.out.println("Rufe serverStub.sayHelloServer() auf");
            serverStub.sayHelloServer();
            
            System.out.println("Bitte einen Text eingeben: ");
            String text = br.readLine();
            
            String antwort = serverStub.antwortAuf(text);
            System.out.println("Antwort vom Server: " + antwort);
            
            //just in case dass das programm nicht von der kommandozeile gestartet wurde:
            System.out.println("---Zum Beenden ENTER drücken---");
            System.in.read();
            
        } catch (RemoteException ex) {
            ex.printStackTrace();
        } catch (NotBoundException ex) {
            System.err.println("Dieser Name ist nicht an die Registry gebunden.");
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
