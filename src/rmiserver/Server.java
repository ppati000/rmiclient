/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rmiserver;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Das Server-Interface.
 * @author Patrick P.
 * @since 0.1
 */
public interface Server extends Remote{
    
        public void sayHelloServer() throws RemoteException;
        
        public String antwortAuf(String s) throws RemoteException;
        
    
}
